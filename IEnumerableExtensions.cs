﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace PolyMapper.Extensions
{
	public static class IEnumerableExtensions
	{
		public static void DoubleForeach<TFirst, TSecond>(IEnumerable<TFirst> firsts, IEnumerable<TSecond> seconds, Action<TFirst, TSecond> action)
		{
			DoubleForeachUntyped(firsts, seconds, (first, second, index) =>
			{
				action((TFirst)first, (TSecond)second);
			});
		}

		public static void DoubleForeach<TFirst, TSecond>(IEnumerable<TFirst> firsts, IEnumerable<TSecond> seconds, Action<TFirst, TSecond, int> action)
		{
			DoubleForeachUntyped(firsts, seconds, (first, second, index) =>
			{
				action((TFirst)first, (TSecond)second, index);
			});
		}

		public static void DoubleForeachUntyped(IEnumerable firsts, IEnumerable seconds, Action<object, object> action)
		{
			DoubleForeachUntyped(firsts, seconds, (first, second, index) =>
			{
				action(first, second);
			});
		}

		public static void DoubleForeachUntyped(IEnumerable firsts, IEnumerable seconds, Action<object, object, int> action)
		{
			var firstEnumerator = firsts.GetEnumerator();
			var secondEnumerator = seconds.GetEnumerator();

			var index = 0;
			while (firstEnumerator.MoveNext() && secondEnumerator.MoveNext())
			{
				action(firstEnumerator.Current, secondEnumerator.Current, index++);
			}
		}

		public static bool IsGenericEnumerable(this Type type)
		{
			return type != typeof(string) && typeof(IEnumerable).IsAssignableFrom(type) && type.GetGenericArguments().Any();
		}

		public static bool EqualsByFunction<T1>(this IEnumerable<T1> first, IEnumerable<T1> second, Func<T1, T1, bool> comparer)
		{
			return first.All(f => second.Any(s => comparer(f, s))) && second.All(s => first.Any(f => comparer(s, f)));
		}
	}
}
