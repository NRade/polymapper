﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace PolyMapper.Extensions
{
	public static class ReflectionFunctions
	{
		/// <summary>
		/// Gets the property name with path.
		/// </summary>
		/// <param name="expression">The expression.</param>
		/// <returns>System.String.</returns>
		public static string GetPropertyNameWithPath(Expression<Func<object>> expression)
		{
			Expression body = expression.Body;
			return GetMemberNameWithPath(body);
		}

		/// <summary>
		/// Gets the property name with path.
		/// </summary>
		/// <param name="expression">The expression.</param>
		/// <returns>System.String.</returns>
		public static string GetPropertyNameWithPath<T>(Expression<Func<T>> expression)
		{
			Expression body = expression.Body;
			return GetMemberNameWithPath(body);
		}

		/// <summary>
		/// Gets the member name with path.
		/// </summary>
		/// <param name="expression">The expression.</param>
		/// <returns>System.String.</returns>
		/// <exception cref="System.Exception">
		/// </exception>
		private static string GetMemberNameWithPath(Expression expression)
		{
			var memberName = String.Empty;
			if (expression is MemberExpression)
			{
				var memberExpression = (MemberExpression)expression;

				if (memberExpression.Expression.NodeType == ExpressionType.MemberAccess || memberExpression.Expression.NodeType == ExpressionType.Call)
				{
					memberName = GetMemberNameWithPath(memberExpression.Expression);
					return memberName == null ? memberExpression.Member.Name : String.Format("{0}.{1}", memberName, memberExpression.Member.Name);
				}

				// Added the exclusion because when\n\r"Expression<Func<object>> expression" is used to access the method, the first variable name is also included in the output.
				//When Using a variable passed as a closure variable, the last expression passed has an inner expression with NodeType as\n\r"Constant". 
				//This expressions member name should not be included as this is the name of the variable which is not required
				//if (memberExpression.Expression.NodeType != ExpressionType.Constant)
				if (memberExpression.Expression.NodeType == ExpressionType.Parameter)
					return memberExpression.Member.Name;

				return null;
			}
			if (expression is UnaryExpression)
			{
				var unaryExpression = (UnaryExpression)expression;

				if (unaryExpression.NodeType != ExpressionType.Convert)
					throw new Exception(String.Format("Cannot interpret member from {0}", expression));

				return GetMemberNameWithPath(unaryExpression.Operand);
			}
			if (expression is MethodCallExpression)
			{
				var methodCallExpression = (MethodCallExpression)expression;
				return GetMemberNameWithPath(methodCallExpression.Object);
			}

			throw new Exception(String.Format("Could not determine member from {0}", expression));
		}

		/// <summary>
		/// determines whether the object has a property at the specified path using reflection
		/// </summary>
		/// <param name="currentType"></param>
		/// <param name="path"></param>
		/// <returns></returns>
		public static bool HasPropertyAtPath(this Type currentType, string path)
		{
			return GetPropertyAtPath(currentType, path) != null;
		}

		/// <summary>
		/// retrieves the object's property at the specified path using reflection
		/// </summary>
		/// <param name="currentType"></param>
		/// <param name="path"></param>
		/// <returns></returns>
		public static PropertyInfo GetPropertyAtPath(this Type currentType, string path)
		{
			PropertyInfo property = null;

			foreach (string currentPropertyName in path.Split('.'))
			{
				var propertyName = currentPropertyName.Replace("[", "").Replace("]", "");

				if (propertyName != "T0")
				{
					property = currentType.GetProperty(propertyName);

					if (property != null)
					{
						if (typeof(IList).IsAssignableFrom(property.PropertyType))
						{
							currentType = property.PropertyType.GetGenericArguments().First();
						}
						else
						{
							currentType = property.PropertyType;
						}
					}
				}
			}
			return property;
		}

		/// <summary>
		/// provide common base class or implemented interface
		/// </summary>
		/// <param name="typeLeft"></param>
		/// <param name="typeRight"></param>
		/// <returns></returns>
		public static Type FindEqualTypeWith(this Type typeLeft, Type typeRight)
		{
			if (typeLeft == null || typeRight == null) return null;

			var commonBaseClass = typeLeft.FindBaseClassWith(typeRight) ?? typeof(object);

			return commonBaseClass.Equals(typeof(object))
					? typeLeft.FindInterfaceWith(typeRight)
					: commonBaseClass;
		}

		/// <summary>
		/// searching for common base class (either concrete or abstract)
		/// </summary>
		/// <param name="typeLeft"></param>
		/// <param name="typeRight"></param>
		/// <returns></returns>
		public static Type FindBaseClassWith(this Type typeLeft, Type typeRight)
		{
			if (typeLeft == null || typeRight == null) return null;

			return typeLeft
					.GetClassHierarchy()
					.Intersect(typeRight.GetClassHierarchy())
					.FirstOrDefault(type => !type.IsInterface);
		}

		/// <summary>
		/// searching for common implemented interface
		/// it's possible for one class to implement multiple interfaces, 
		/// in this case return first common based interface
		/// </summary>
		/// <param name="typeLeft"></param>
		/// <param name="typeRight"></param>
		/// <returns></returns>
		public static Type FindInterfaceWith(this Type typeLeft, Type typeRight)
		{
			if (typeLeft == null || typeRight == null) return null;

			return typeLeft
					.GetInterfaceHierarchy()
					.Intersect(typeRight.GetInterfaceHierarchy())
					.FirstOrDefault();
		}

		/// <summary>
		/// iterate on interface hierarhy
		/// </summary>
		/// <param name="type"></param>
		/// <returns></returns>
		public static IEnumerable<Type> GetInterfaceHierarchy(this Type type)
		{
			if (type.IsInterface) return new[] { type }.AsEnumerable();

			return type
					.GetInterfaces()
					.OrderByDescending(current => current.GetInterfaces().Count())
					.AsEnumerable();
		}

		/// <summary>
		/// interate on class hierarhy
		/// </summary>
		/// <param name="type"></param>
		/// <returns></returns>
		public static IEnumerable<Type> GetClassHierarchy(this Type type)
		{
			if (type == null) yield break;

			Type typeInHierarchy = type;

			do
			{
				yield return typeInHierarchy;
				typeInHierarchy = typeInHierarchy.BaseType;
			}
			while (typeInHierarchy != null && !typeInHierarchy.IsInterface);
		}
	}
}
