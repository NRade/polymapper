﻿using System.Collections.Generic;

namespace PolyMapper.Extensions
{
	/// <summary>
	/// extensions to the System.Collections.Generic.Dictionary class
	/// </summary>
	static class DictionaryExtensions
	{
		/// <summary>
		/// gets the value at the given index or adds a new entry at the index and returns it
		/// </summary>
		/// <typeparam name="T1"></typeparam>
		/// <typeparam name="T2"></typeparam>
		/// <typeparam name="T3"></typeparam>
		/// <param name="dictionary"></param>
		/// <param name="inputValue"></param>
		/// <returns></returns>
		public static T3 GetValueOrAddedNew<T1, T2, T3>(this Dictionary<T1, T2> dictionary, T1 inputValue) where T3 : T2, new()
		{
			if (!dictionary.TryGetValue(inputValue, out T2 outputValue))
			{
				outputValue = new T3();
				dictionary.Add(inputValue, outputValue);
			}
			return (T3)outputValue;
		}

		/// <summary>
		/// gets the value at the given index or adds a new entry at the index and returns it
		/// </summary>
		/// <typeparam name="T1"></typeparam>
		/// <typeparam name="T2"></typeparam>
		/// <param name="dictionary"></param>
		/// <param name="inputValue"></param>
		/// <returns></returns>
		public static T2 GetValueOrAddedNew<T1, T2>(this Dictionary<T1, T2> dictionary, T1 inputValue) where T2 : new()
		{
			return GetValueOrAddedNew<T1, T2, T2>(dictionary, inputValue);
		}
	}
}
