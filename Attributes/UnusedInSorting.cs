﻿using System;
namespace PolyMapper.Attributes
{
	/// <summary>
	/// denotes a mapping that is unused in sorting/filtering
	/// </summary>
	public class UnusedInSorting : Attribute
	{
	}
}
