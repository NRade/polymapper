﻿using PolyMapper.Extensions;
using System;
using System.Linq.Expressions;

namespace PolyMapper.Attributes
{
	/// <summary>
	/// denotes that the property of a model is hard to sort and enables use of client side sorting/filtering
	/// as opposed to using data layer sorting and filtering
	/// </summary>
	public class HardSortable : Attribute
	{
		/// <summary>
		/// property name
		/// </summary>
		public string PropertyName = "";

		/// <summary>
		/// empty constructor
		/// </summary>
		public HardSortable()
		{

		}

		/// <summary>
		/// constructor with given property name
		/// </summary>
		/// <param name="propertyName"></param>
		public HardSortable(string propertyName)
		{
			PropertyName = propertyName;
		}

		/// <summary>
		/// constructor with given property name as expression
		/// </summary>
		/// <param name="propertyName"></param>
		public HardSortable(Expression<Func<object>> propertyName)
		{
			ReflectionFunctions.GetPropertyNameWithPath(propertyName);
		}
	}
}
