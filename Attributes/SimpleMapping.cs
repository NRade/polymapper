﻿using System;
namespace PolyMapper.Attributes
{
	/// <summary>
	/// denotes a simple property mapping for model mapping
	/// </summary>
	public class SimpleMapping : Attribute
	{
	}
}
