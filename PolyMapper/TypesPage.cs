﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PolyMapper
{
    /// <summary>
    /// types page for model mapping
    /// </summary>
    public class TypesPage<TPageType> : Dictionary<Type, TPageType>
    {
    }
}