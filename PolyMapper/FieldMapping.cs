﻿using PolyMapper.Extensions;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace PolyMapper
{
	/// <summary>
	/// field group for model mapping
	/// </summary>
	public class FieldMapping : List<string>
	{
		/// <summary>
		/// initialize based on expression parameters
		/// </summary>
		/// <param name="expressions"></param>
		public FieldMapping(params Expression<Func<object>>[] expressions)
		{
			foreach (var expression in expressions)
			{
				Add(ReflectionFunctions.GetPropertyNameWithPath(expression));
			}
		}
	}
}
