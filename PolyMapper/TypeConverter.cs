﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PolyMapper
{
    public class TypeConverter<T1, T2> : TypeConverter
    {
        private Func<T1, T2> conversionFunction;

        public TypeConverter(Func<T1, T2> conversionFunction)
        {
            this.conversionFunction = conversionFunction;
        }

        public T2 Convert(T1 input)
        {
            return conversionFunction(input);
        }

        public object Convert(object input)
        {
            return conversionFunction((T1)input);
        }
    }

    interface TypeConverter
    {
        object Convert(object input);
    }
}
