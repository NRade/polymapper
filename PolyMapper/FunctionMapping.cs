﻿using PolyMapper.Extensions;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace PolyMapper
{
	/// <summary>
	/// complex property mapping based on delegate Action
	/// </summary>
	public class FunctionMapping
	{
		/// <summary>
		/// attribute of this mapping; suitable examples: HardSortable, UnusedInSorting
		/// </summary>
		public Attribute Attribute;

		protected Action<IEnumerable, IEnumerable, PropertyChecker> MappingAction;

		/// <summary>
		/// map properties between single models
		/// </summary>
		/// <param name="sourceModel"></param>
		/// <param name="targetModel"></param>
		/// <param name="propertyChecker"></param>
		public void MapSingle(object sourceModel, object targetModel, PropertyChecker propertyChecker = null)
		{
			propertyChecker = propertyChecker ?? new PropertyChecker(InclusionMode.IncludeAll);

			MappingAction(new List<object> { sourceModel }, new List<object> { targetModel }, propertyChecker);
		}

		/// <summary>
		/// map properties between single models
		/// </summary>
		/// <param name="sourceModels"></param>
		/// <param name="targetModels"></param>
		/// <param name="propertyChecker"></param>
		public void MapMultiple(IEnumerable sourceModels, IEnumerable targetModels, PropertyChecker propertyChecker = null)
		{
			propertyChecker = propertyChecker ?? new PropertyChecker(InclusionMode.IncludeAll);

			MappingAction(sourceModels, targetModels, propertyChecker);
		}

		/// <summary>
		/// complex mapping based on mapping action between single objects
		/// </summary>
		/// <param name="mappingAction"></param>
		/// <param name="attribute"></param>
		public FunctionMapping(Action<object, object> mappingAction, Attribute attribute = null)
		{
			MappingAction = new Action<IEnumerable, IEnumerable, PropertyChecker>((sources, targets, propertyChecker) =>
			{
				if (propertyChecker.InclusionMode != InclusionMode.Exclude)
				{
					IEnumerableExtensions.DoubleForeachUntyped(sources, targets, mappingAction);
				}
			});

			Attribute = attribute;
		}

		/// <summary>
		/// complex mapping based on mapping action between multiple objects
		/// </summary>
		/// <param name="mappingAction"></param>
		/// <param name="attribute"></param>
		public FunctionMapping(Action<IEnumerable, IEnumerable> mappingAction, Attribute attribute = null)
		{
			MappingAction = (sources, targets, propertyChecker) =>
			{
				if (propertyChecker.InclusionMode != InclusionMode.Exclude)
				{
					mappingAction(sources, targets);
				}
			};

			Attribute = attribute;
		}

		/// <summary>
		/// complex mapping based on mapping action between single objects
		/// </summary>
		/// <param name="mappingAction"></param>
		/// <param name="attribute"></param>
		public FunctionMapping(Action<object, object, PropertyChecker> mappingAction, Attribute attribute = null)
		{
			MappingAction = (sources, targets, propertyChecker) =>
			{
				IEnumerableExtensions.DoubleForeachUntyped(sources, targets, (source, target) =>
				{
					mappingAction(source, target, propertyChecker);
				});
			};

			Attribute = attribute;
		}

		/// <summary>
		/// complex mapping based on mapping action between multiple objects
		/// </summary>
		/// <param name="mappingAction"></param>
		/// <param name="attribute"></param>
		public FunctionMapping(Action<IEnumerable, IEnumerable, PropertyChecker> mappingAction, Attribute attribute = null)
		{
			MappingAction = mappingAction;

			Attribute = attribute;
		}
	}

	public class FunctionMapping<TSource, TTarget> : FunctionMapping
	{
		/// <summary>
		/// complex mapping based on mapping action between single objects
		/// </summary>
		/// <param name="mappingAction"></param>
		/// <param name="attribute"></param>
		public FunctionMapping(Action<TSource, TTarget> mappingAction, Attribute attribute = null)
			: base(new Action<object, object>((sourceObject, targetObject) => mappingAction((TSource)sourceObject, (TTarget)targetObject)), attribute)
		{
		}

		/// <summary>
		/// complex mapping based on mapping action between multiple objects
		/// </summary>
		/// <param name="mappingAction"></param>
		/// <param name="attribute"></param>
		public FunctionMapping(Action<List<TSource>, List<TTarget>> mappingAction, Attribute attribute = null)
			: base(new Action<IEnumerable, IEnumerable>((sourceObject, targetObject) => mappingAction(sourceObject.Cast<TSource>().ToList(), targetObject.Cast<TTarget>().ToList())), attribute)
		{
		}

		/// <summary>
		/// complex mapping based on mapping action between single objects
		/// </summary>
		/// <param name="mappingAction"></param>
		/// <param name="attribute"></param>
		public FunctionMapping(Action<TSource, TTarget, PropertyChecker> mappingAction, Attribute attribute = null)
			: base(new Action<object, object, PropertyChecker>((sourceObject, targetObject, propertyChecker) => mappingAction((TSource)sourceObject, (TTarget)targetObject, propertyChecker)), attribute)
		{
		}

		/// <summary>
		/// complex mapping based on mapping action between multiple objects
		/// </summary>
		/// <param name="mappingAction"></param>
		/// <param name="attribute"></param>
		public FunctionMapping(Action<List<TSource>, List<TTarget>, PropertyChecker> mappingAction, Attribute attribute = null)
			: base(new Action<IEnumerable, IEnumerable, PropertyChecker>((sourceList, targetList, propertyChecker) => mappingAction(sourceList.Cast<TSource>().ToList(), targetList.Cast<TTarget>().ToList(), propertyChecker)), attribute)
		{
		}
	}
}
