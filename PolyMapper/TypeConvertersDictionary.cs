﻿using PolyMapper.Extensions;
using System;
using System.Collections.Generic;

namespace PolyMapper
{
	/// <summary>
	/// fast access dictionary structure with type converters
	/// </summary>
	public class TypeConvertersDictionary
	{
		private Dictionary<Type, TypesPage<TypeConverter>> TypesDictionary = new Dictionary<Type, TypesPage<TypeConverter>>();

		/// <summary>
		/// try to get an object converted to the target type based on existing type conversions
		/// </summary>
		/// <typeparam name="T1"></typeparam>
		/// <typeparam name="T2"></typeparam>
		/// <param name="inputObject"></param>
		/// <param name="convertedObject"></param>
		/// <returns></returns>
		public bool TryGetConvertedObject<T1, T2>(T1 inputObject, out T2 convertedObject)
		{
			if (TypesDictionary.TryGetValue(typeof(T1), out TypesPage<TypeConverter> typesPage) &&
				typesPage.TryGetValue(typeof(T2), out TypeConverter typeConverter))
			{
				convertedObject = ((TypeConverter<T1, T2>)typeConverter).Convert(inputObject);
				return true;
			}
			convertedObject = default(T2);
			return false;
		}

		/// <summary>
		/// try to get an object converted to the target type based on existing type conversions
		/// </summary>
		/// <param name="targetType"></param>
		/// <param name="inputObject"></param>
		/// <param name="convertedObject"></param>
		/// <returns></returns>
		public bool TryGetConvertedObject(Type sourceType, Type targetType, object inputObject, out object convertedObject)
		{
			if (TypesDictionary.TryGetValue(sourceType, out TypesPage<TypeConverter> typesPage) &&
				typesPage.TryGetValue(targetType, out TypeConverter typeConverter))
			{
				if (typeConverter != null)
				{
					convertedObject = typeConverter.Convert(inputObject);
					return true;
				}
				else
				{
					convertedObject = null;
					return false;
				}
			}

			// for conversions from Nullable<T> to T
			if (targetType == inputObject.GetType())
			{
				var converter = new TypeConverter<object, object>(o => o);

				AddConverter(sourceType, targetType, converter);

				convertedObject = inputObject;
				return true;
			}

			if (sourceType.IsValueType)
			{
				var nullableType = typeof(Nullable<>).MakeGenericType(sourceType);
				if (targetType == nullableType)
				{
					var converter = new TypeConverter<object, object>(o => Activator.CreateInstance(nullableType, o));

					AddConverter(sourceType, targetType, converter);

					convertedObject = converter.Convert(inputObject);
					return true;
				}
			}

			if (typeof(IConvertible).IsAssignableFrom(sourceType) && inputObject != null)
			{
				var converter = new TypeConverter<object, object>(o => Convert.ChangeType(o, targetType));

				AddConverter(sourceType, targetType, converter);

				convertedObject = converter.Convert(inputObject);
				return true;
			}

			convertedObject = null;

			// put info in TypesDictionary that conversion is impossible
			// so it does not need to be tried again between these types
			AddConverter(sourceType, targetType, null);
			return false;
		}

		/// <summary>
		/// add a type converter
		/// </summary>
		/// <typeparam name="T1"></typeparam>
		/// <typeparam name="T2"></typeparam>
		/// <param name="converter"></param>
		public void AddConverter<T1, T2>(TypeConverter<T1, T2> converter)
		{
			AddConverter(typeof(T1), typeof(T2), new TypeConverter<object, object>((o1) => converter.Convert(o1)));
		}

		public void AddConverter(Type t1, Type t2, TypeConverter<object, object> converter)
		{
			var typesPage = TypesDictionary.GetValueOrAddedNew(t1);
			typesPage.Add(t2, converter);
		}
	}
}
