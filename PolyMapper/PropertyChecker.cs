﻿using PolyMapper.Extensions;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace PolyMapper
{
	/// <summary>
	/// inclusion mode for property checker
	/// </summary>
	public enum InclusionMode
	{
		/// <summary>
		/// logic set to include properties that were specified in the propertyChecker's constructor
		/// </summary>
		Include,
		/// <summary>
		/// logic set to exclude properties that were specified in the propertyChecker's constructor
		/// </summary>
		Exclude,
		/// <summary>
		/// logic set to include all properties regardless of which were set in the propertyChecker's constructor
		/// </summary>
		IncludeAll
	}

	/// <summary>
	/// class that wraps logic around a Func{string, bool} delegate
	/// 
	/// used to determine whether properties are included or excluded when mapping them from model to model
	/// based on the logic in the Func{string, bool}
	/// </summary>
	public class PropertyChecker
	{
		/// <summary>
		/// specifies whether properties are being included or excluded, or all are included (see enum InclusionMode)
		/// </summary>
		public InclusionMode InclusionMode;

		/// <summary>
		/// logic for determining whether a property is included or excluded (in Included mode)
		/// </summary>
		private Func<string, bool> inclusionFunction;

		/// <summary>
		/// empty constructor
		/// </summary>
		public PropertyChecker(InclusionMode inclusionMode = InclusionMode.IncludeAll)
		{
			InclusionMode = inclusionMode;
		}

		/// <summary>
		/// constructor specifying included properties (in Included mode)
		/// </summary>
		/// <param name="properties"></param>
		/// <param name="inclusionMode"></param>
		public PropertyChecker(List<string> properties, InclusionMode inclusionMode = InclusionMode.IncludeAll)
		{
			InclusionMode = inclusionMode;
			inclusionFunction = (s) => properties.Contains(s);
		}

		/// <summary>
		/// determines whether the property is included
		/// </summary>
		/// <param name="property"></param>
		/// <returns></returns>
		public bool IsIncluded(string property)
		{
			return InclusionMode == InclusionMode.IncludeAll || (InclusionMode == InclusionMode.Include) ^ inclusionFunction(property);
		}

		/// <summary>
		/// determines whether the property denoted by the expression is included
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="expression"></param>
		/// <returns></returns>
		public bool IsIncluded<T>(Expression<Func<T>> expression)
		{
			return IsIncluded(ReflectionFunctions.GetPropertyNameWithPath(expression));
		}
	}
}
