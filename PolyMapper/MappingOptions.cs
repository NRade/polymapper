﻿using PolyMapper.Attributes;
using System;
using System.Collections.Generic;
using System.Reflection;

namespace PolyMapper
{
	public class MappingOptions
	{
		public SameNameMappingMode? SameNameMapping;

		public ListMappingMode? ListMapping;

		public ReferenceTypeMappingMode? ReferenceTypeMapping;

		public int? MappingDepth;

		public List<Attribute> IncludedAttributes;

		public static MappingOptions GetDefaultMappingOptions()
		{
			var newOptions = new MappingOptions();
			newOptions.SetDefaults();
			return newOptions;
		}

		public void SetDefaults()
		{
			SameNameMapping = SameNameMappingMode.IgnoreCase;
			ListMapping = ListMappingMode.Ignore;
			MappingDepth = int.MaxValue;
			ReferenceTypeMapping = ReferenceTypeMappingMode.Ignore;
			IncludedAttributes = new List<Attribute> { new HardSortable(), new UnusedInSorting(), new SimpleMapping() };
		}

		public MappingOptions Merge(MappingOptions otherMappingOptions)
		{
			if (otherMappingOptions != null)
			{
				foreach (var field in typeof(MappingOptions).GetFields(BindingFlags.Public | BindingFlags.Instance))
				{
					var otherValue = field.GetValue(otherMappingOptions);

					if (otherValue != null)
					{
						field.SetValue(this, otherValue);
					}
				}
			}

			return this;
		}
	}

	public enum SameNameMappingMode
	{
		CaseSensitive,
		IgnoreCase,
		Disabled
	}

	public enum ListMappingMode
	{
		MapByValues,
		MapByReference,
		Ignore
	}

	public enum ReferenceTypeMappingMode
	{
		Clone,
		MapByReference,
		Ignore
	}
}
