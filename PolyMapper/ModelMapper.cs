﻿using PolyMapper.Attributes;
using PolyMapper.Extensions;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace PolyMapper
{
	/// <summary>
	/// model mapper class for mapping properties between models
	/// </summary>
	public class ModelMapper
	{
		/// <summary>
		/// mappings between properties of different models in a fast-access dictionary structure
		/// </summary>
		private FieldMappingsDictionary Mappings = new FieldMappingsDictionary();

		/// <summary>
		/// type converters for mappings between properties of different types
		/// </summary>
		private TypeConvertersDictionary TypeConverters = new TypeConvertersDictionary();

        /// <summary>
        /// known types from all loaded assemblies
        /// </summary>
        private static List<Type> KnownTypes;

        /// <summary>
        /// mapping options that apply for all mappings for which none have specifically been defined
        /// </summary>
        public MappingOptions DefaultMappingOptions => Mappings.DefaultMappingOptions;

        static ModelMapper()
        {
            KnownTypes = AppDomain.CurrentDomain.GetAssemblies().SelectMany(asm =>
            {
                try
                {
                    return asm.GetTypes();
                }
                catch
                {
                    return new Type[0];
                }
            }).ToList();
        }

		/// <summary>
		/// add simple property mapping placeholder
		/// 
		/// adds mapping from the type to itself; this can be used to specify that a model has certain properties, without mapping them to another model
		/// </summary>
		/// <typeparam name="T1"></typeparam>
		/// <param name="fieldgroups"></param>
		public void Add<T1>(params FieldMapping[] fieldgroups)
		{
			foreach (var fieldGroup in fieldgroups)
			{
				Mappings.Add<T1, T1>(fieldGroup[0], fieldGroup[0], true);
			}
		}

		/// <summary>
		/// add simple property mappings between two types
		/// </summary>
		/// <typeparam name="T1"></typeparam>
		/// <typeparam name="T2"></typeparam>
		/// <param name="fieldgroups"></param>
		public void Add<T1, T2>(params FieldMapping[] fieldgroups)
		{
			foreach (var fieldGroup in fieldgroups)
			{
				if (fieldGroup.Count >= 2)
				{
					Mappings.Add<T1, T2>(fieldGroup[0], fieldGroup[1], true);
				}
			}
		}

		/// <summary>
		/// add simple property mappings between three types
		/// </summary>
		/// <typeparam name="T1"></typeparam>
		/// <typeparam name="T2"></typeparam>
		/// <typeparam name="T3"></typeparam>
		/// <param name="fieldgroups"></param>
		public void Add<T1, T2, T3>(params FieldMapping[] fieldgroups)
		{
			Add<T1, T2>(fieldgroups);

			foreach (var fieldGroup in fieldgroups)
			{
				if (fieldGroup.Count >= 3)
				{
					Mappings.Add<T1, T3>(fieldGroup[0], fieldGroup[2], true);
					Mappings.Add<T2, T3>(fieldGroup[1], fieldGroup[2], true);
				}
			}
		}

		/// <summary>
		/// add simple property mappings between four types
		/// </summary>
		/// <typeparam name="T1"></typeparam>
		/// <typeparam name="T2"></typeparam>
		/// <typeparam name="T3"></typeparam>
		/// <typeparam name="T4"></typeparam>
		/// <param name="fieldgroups"></param>
		public void Add<T1, T2, T3, T4>(params FieldMapping[] fieldgroups)
		{
			Add<T1, T2, T3>(fieldgroups);

			foreach (var fieldGroup in fieldgroups)
			{
				if (fieldGroup.Count >= 4)
				{
					Mappings.Add<T1, T4>(fieldGroup[0], fieldGroup[3], true);
					Mappings.Add<T2, T4>(fieldGroup[1], fieldGroup[3], true);
					Mappings.Add<T3, T4>(fieldGroup[2], fieldGroup[3], true);
				}
			}
		}

		/// <summary>
		/// add simple property mappings between five types
		/// </summary>
		/// <typeparam name="T1"></typeparam>
		/// <typeparam name="T2"></typeparam>
		/// <typeparam name="T3"></typeparam>
		/// <typeparam name="T4"></typeparam>
		/// <typeparam name="T5"></typeparam>
		/// <param name="fieldgroups"></param>
		public void Add<T1, T2, T3, T4, T5>(params FieldMapping[] fieldgroups)
		{
			Add<T1, T2, T3, T4>(fieldgroups);

			foreach (var fieldGroup in fieldgroups)
			{
				if (fieldGroup.Count >= 5)
				{
					Mappings.Add<T1, T5>(fieldGroup[0], fieldGroup[4], true);
					Mappings.Add<T2, T5>(fieldGroup[1], fieldGroup[4], true);
					Mappings.Add<T3, T5>(fieldGroup[2], fieldGroup[4], true);
					Mappings.Add<T4, T5>(fieldGroup[3], fieldGroup[4], true);
				}
			}
		}

		/// <summary>
		/// add type converter
		/// </summary>
		/// <typeparam name="TSource"></typeparam>
		/// <typeparam name="TTarget"></typeparam>
		/// <param name="typeConverter"></param>
		public void AddTypeConverter<TSource, TTarget>(TypeConverter<TSource, TTarget> typeConverter)
		{
			TypeConverters.AddConverter(typeConverter);
		}

		/// <summary>
		/// add type converter based on given type conversion function
		/// </summary>
		/// <typeparam name="TSource"></typeparam>
		/// <typeparam name="TTarget"></typeparam>
		/// <param name="typeConversionFunction"></param>
		public void AddTypeConverter<TSource, TTarget>(Func<TSource, TTarget> typeConversionFunction)
		{
			TypeConverters.AddConverter(new TypeConverter<TSource, TTarget>(typeConversionFunction));
		}

		/// <summary>
		/// add a complex mapping
		/// </summary>
		/// <typeparam name="TSource"></typeparam>
		/// <typeparam name="TTarget"></typeparam>
		/// <param name="functionMapping"></param>
		public void Add<TSource, TTarget>(FunctionMapping<TSource, TTarget> functionMapping)
		{
			Mappings.AddFunctionMapping(functionMapping);
		}

		/// <summary>
		/// add a function-based mapping between lists of objects; this may have performance advantages over
		/// using a function-based mapping between single objects
		/// </summary>
		/// <typeparam name="TSource"></typeparam>
		/// <typeparam name="TTarget"></typeparam>
		/// <param name="functionMapping"></param>
		public void AddMultiMapping<TSource, TTarget>(Action<List<TSource>, List<TTarget>> functionMapping)
		{
			Mappings.AddFunctionMapping(new FunctionMapping<TSource, TTarget>(functionMapping));
		}

		/// <summary>
		/// map all HardSortable properties in the inclusion list for properties between the models
		/// </summary>
		/// <typeparam name="TSource"></typeparam>
		/// <typeparam name="TTarget"></typeparam>
		/// <param name="sourceModels"></param>
		/// <param name="targetModels"></param>
		/// <param name="propertyChecker"></param>
		public void MapForHardSortables<TSource, TTarget>(List<TSource> sourceModels, List<TTarget> targetModels, PropertyChecker propertyChecker)
		{
			var fieldsPage = Mappings.GetFieldsPage<TSource, TTarget>();

			foreach (var functionMapping in fieldsPage.FunctionMappings)
			{
				if (functionMapping.Attribute is HardSortable && propertyChecker.IsIncluded(((HardSortable)functionMapping.Attribute).PropertyName))
				{
					functionMapping.MapMultiple(sourceModels, targetModels, propertyChecker);
				}
			}
		}

		/// <summary>
		/// map all properties between two lists of models
		/// </summary>
		/// <typeparam name="TSource"></typeparam>
		/// <typeparam name="TTarget"></typeparam>
		/// <param name="sourceModels"></param>
		/// <param name="targetModels"></param>
		/// <param name="includedAttributes"></param>
		public void MapModels<TSource, TTarget>(List<TSource> sourceModels, List<TTarget> targetModels, MappingOptions mappingOptions = null)
		{
			MapModels(typeof(TSource), typeof(TTarget), sourceModels, targetModels, mappingOptions);
		}

		/// <summary>
		/// map all properties between two lists of models
		/// </summary>
		/// <param name="sourceType"></param>
		/// <param name="targetType"></param>
		/// <param name="sourceModels"></param>
		/// <param name="targetModels"></param>
		/// <param name="includedAttributes"></param>
		public void MapModels(Type sourceType, Type targetType, ICollection sourceModels, ICollection targetModels, MappingOptions mappingOptions = null)
		{
			var fieldsPage = Mappings.GetFieldsPage(sourceType, targetType);

			mappingOptions = DefaultMappingOptions.Merge(fieldsPage.Options.Merge(mappingOptions));

			if (mappingOptions.IncludedAttributes.Contains(new SimpleMapping()))
			{
				var commonPropertiesToBeMapped = new Dictionary<string, string>();

				var commonAncestorType = sourceType.FindEqualTypeWith(targetType);

				// if there is a common ancestor, map shared fields by default
				if (commonAncestorType != null)
				{
					foreach (var name in commonAncestorType.GetProperties(BindingFlags.Public | BindingFlags.Instance).Select(p => p.Name))
					{
						commonPropertiesToBeMapped.Add(name, name);
					}
				}

				foreach (var mapping in fieldsPage.GetSameNameMappings())
				{
					if (!commonPropertiesToBeMapped.ContainsKey(mapping.Key))
					{
						commonPropertiesToBeMapped.Add(mapping.Key, mapping.Value);
					}
				}

				foreach (var commonProperty in commonPropertiesToBeMapped)
				{
					IEnumerableExtensions.DoubleForeachUntyped(sourceModels, targetModels, (source, target) =>
					{
						TransferValue(source, target, commonProperty.Key, commonProperty.Value, mappingOptions);
					});
				}

				// mappings may override mappings between common ancestor type's fields
				foreach (var mapping in fieldsPage.PropertyMappings)
				{
					IEnumerableExtensions.DoubleForeachUntyped(sourceModels, targetModels, (source, target) =>
					{
						TransferValue(source, target, mapping.Key, mapping.Value, mappingOptions);
					});
				}
			}

			foreach (var functionMapping in fieldsPage.FunctionMappings)
			{
				if (functionMapping.Attribute == null || mappingOptions.IncludedAttributes.Any(a => a.GetType() == functionMapping.Attribute.GetType()))
				{
					functionMapping.MapMultiple(sourceModels, targetModels);
				}
			}
		}

		/// <summary>
		/// map all properties between two models
		/// </summary>
		/// <typeparam name="TSource"></typeparam>
		/// <typeparam name="TTarget"></typeparam>
		/// <param name="sourceModel"></param>
		/// <param name="targetModel"></param>
		/// <param name="propertyAttributes"></param>
		public void MapModel<TSource, TTarget>(TSource sourceModel, TTarget targetModel, MappingOptions mappingOptions = null)
		{
			MapModels(new List<TSource> { sourceModel }, new List<TTarget> { targetModel }, mappingOptions);
		}

		/// <summary>
		/// create model based on model mapping
		/// </summary>
		/// <typeparam name="TSource"></typeparam>
		/// <typeparam name="TOutput"></typeparam>
		/// <param name="sourceModel"></param>
		/// <param name="propertyAttributes"></param>
		/// <returns></returns>
		public TOutput CreateModel
			<TSource, TOutput>(TSource sourceModel, MappingOptions mappingOptions = null) where TOutput : new()
		{
			if (sourceModel == null)
			{
				return default(TOutput);
			}

			var targetModel = new TOutput();
			MapModel(sourceModel, targetModel, mappingOptions);
			return targetModel;
		}



		/// <summary>
		/// create models based on model mapping
		/// </summary>
		/// <typeparam name="TSource"></typeparam>
		/// <typeparam name="TOutput"></typeparam>
		/// <param name="sourceModels"></param>
		/// <param name="propertyAttributes"></param>
		/// <returns></returns>
		public List<TOutput> CreateModels<TSource, TOutput>(List<TSource> sourceModels, MappingOptions mappingOptions = null) where TOutput : new()
		{
			return CreateModels(typeof(TSource), typeof(TOutput), typeof(List<TOutput>), sourceModels, mappingOptions).Cast<TOutput>().ToList();
		}

		public IList CreateModels(Type sourceType, Type outputType, Type collectionOutputType, ICollection sourceModels, MappingOptions mappingOptions = null)
		{
			var listType = collectionOutputType.GetGenericArguments().Any() ? collectionOutputType : collectionOutputType.MakeGenericType(new Type[] { outputType });
            if (listType.GetGenericTypeDefinition() == typeof(IEnumerable<>)) listType = typeof(List<>).MakeGenericType(new Type[] { outputType });

            var outputModels = (IList)Activator.CreateInstance(listType);
			foreach (var sourceModel in sourceModels)
			{
                var instanceType = outputType.IsInterface ? KnownTypes.First(t => t.IsClass && !t.IsAbstract && outputType.IsAssignableFrom(t)) : outputType;
				outputModels.Add(Activator.CreateInstance(instanceType));
			}
			MapModels(sourceType, outputType, sourceModels, outputModels, mappingOptions);
			return outputModels;
		}

		public T DeepCloneObject<T>(T source) where T : new()
		{
			return CreateModel<T, T>(source, new MappingOptions { ListMapping = ListMappingMode.MapByValues });
		}

		/// <summary>
		/// set mode of mapping of properties by the same name between the given two types
		/// 
		/// case insensitive mapping enabled by default
		/// </summary>
		/// <typeparam name="TSource"></typeparam>
		/// <typeparam name="TTarget"></typeparam>
		/// <param name="bothWays"></param>
		/// <param name="mode"></param>
		public void SetOptions<TSource, TTarget>(MappingOptions mappingOptions, bool bothWays = false)
		{
			Mappings.SetOptions<TSource, TTarget>(mappingOptions, bothWays);
		}

		public MappingOptions GetOptions<TSource, TTarget>()
		{
			return Mappings.GetOptions<TSource, TTarget>();
		}

		/// <summary>
		/// map all properties of the same name between the given models
		/// </summary>
		/// <typeparam name="TSource"></typeparam>
		/// <typeparam name="TTarget"></typeparam>
		/// <param name="source"></param>
		/// <param name="target"></param>
		/// <param name="mode"></param>
		public static void DirectMap<TSource, TTarget>(TSource source, TTarget target, MappingOptions mappingOptions = null)
		{
			var modelMapper = new ModelMapper();
			modelMapper.MapModel(source, target, mappingOptions);
		}

		/// <summary>
		/// create new target object and map all properties of the same name from source object to target object
		/// </summary>
		/// <typeparam name="TSource"></typeparam>
		/// <typeparam name="TTarget"></typeparam>
		/// <param name="source"></param>
		/// <param name="mode"></param>
		/// <returns></returns>
		public static TTarget DirectCreate<TSource, TTarget>(TSource source, MappingOptions mappingOptions = null) where TTarget : new()
		{
			var target = new TTarget();
			DirectMap(source, target, mappingOptions);
			return target;
		}

		/// <summary>
		/// transfer property value from source object to target object, from source path to target path
		/// uses reflection; may be performance intensive when used in nested loops
		/// </summary>
		/// <param name="sourceObject"></param>
		/// <param name="targetObject"></param>
		/// <param name="sourcePath"></param>
		/// <param name="targetPath"></param>
		/// <param name="typeConvertersDictionary"></param>
		private void TransferValue(object sourceObject, object targetObject, string sourcePath, string targetPath = null, MappingOptions mappingOptions = null)
		{
			targetPath = targetPath ?? sourcePath;
			mappingOptions = mappingOptions ?? MappingOptions.GetDefaultMappingOptions();

			var sourceCurrentObject = sourceObject;

			PropertyInfo sourceCurrentProperty = null;

			foreach (string propertyName in sourcePath.Split('.'))
			{
				if (sourceCurrentObject == null)
				{
					return;
				}

				sourceCurrentProperty = sourceCurrentObject.GetType().GetProperty(propertyName);

				sourceCurrentObject = sourceCurrentProperty.GetValue(sourceCurrentObject, null);
			}

			var targetCurrentObject = targetObject;

			PropertyInfo targetCurrentProperty;

			var splitTargetPath = targetPath.Split('.');
			for (int i = 0; i < splitTargetPath.Length; i++)
			{
				if (targetCurrentObject == null)
				{
					return;
				}

				targetCurrentProperty = targetCurrentObject.GetType().GetProperty(splitTargetPath[i]);

				if (i < splitTargetPath.Length - 1)
				{
					targetCurrentObject = targetCurrentProperty.GetValue(targetCurrentObject, null);
				}
				else
				{
					if (sourceCurrentProperty != null && sourceCurrentObject != null)
					{
						if (mappingOptions.ListMapping == ListMappingMode.MapByValues && mappingOptions.MappingDepth > 0 && sourceCurrentProperty.PropertyType.IsGenericEnumerable())
						{
							var sourceGenericType = sourceCurrentProperty.PropertyType.GetGenericArguments()[0];
							var targetGenericType = targetCurrentProperty.PropertyType.GetGenericArguments()[0];

							var sourceCurrentList = ((IEnumerable)sourceCurrentObject).OfType<object>().ToList();

							var newMappingOptions = DirectCreate<MappingOptions, MappingOptions>(mappingOptions);
							newMappingOptions.MappingDepth--;

                            if (targetCurrentProperty.CanWrite)
                            {
                                var targetList = CreateModels(sourceGenericType, targetGenericType, targetCurrentProperty.PropertyType, sourceCurrentList, newMappingOptions);

                                targetCurrentProperty.SetValue(targetCurrentObject, targetList);
                            }
						}
						else
						{
							if (targetCurrentProperty.PropertyType != sourceCurrentProperty.PropertyType)
							{
								if (!TypeConverters.TryGetConvertedObject(sourceCurrentProperty.PropertyType, targetCurrentProperty.PropertyType, sourceCurrentObject, out sourceCurrentObject))
								{
									return;
								}
							}

							if (targetCurrentProperty.CanWrite && ((mappingOptions.ListMapping == ListMappingMode.MapByReference && mappingOptions.MappingDepth > 0) || !sourceCurrentProperty.PropertyType.IsGenericEnumerable()))
							{
								targetCurrentProperty.SetValue(targetCurrentObject, sourceCurrentObject, null);
							}
						}
					}
				}
			}
		}
	}
}
