﻿using PolyMapper.Extensions;
using System;
using System.Collections.Generic;

namespace PolyMapper
{
	/// <summary>
	/// field mappings dictionary for model mapping
	/// </summary>
	public class FieldMappingsDictionary
	{
		private Dictionary<Type, TypesPage<FieldsPage>> mappingsDictionary = new Dictionary<Type, TypesPage<FieldsPage>>();

        /// <summary>
        /// mapping options that apply for all mappings for which none have specifically been defined
        /// </summary>
        public MappingOptions DefaultMappingOptions { get; set; } = MappingOptions.GetDefaultMappingOptions();

        /// <summary>
        /// try to get the field mapping from given source type to target type
        /// </summary>
        /// <typeparam name="TSource"></typeparam>
        /// <typeparam name="TTarget"></typeparam>
        /// <param name="sourceField"></param>
        /// <param name="mappedField"></param>
        /// <returns></returns>
        public bool TryGetMappedField<TSource, TTarget>(string sourceField, out string mappedField)
		{
			// check if the models share a property based on inheritance relations already;
			// in this case the property name stays the same and we do not need to find a mapping
			var commonAncestorType = typeof(TSource).FindEqualTypeWith(typeof(TTarget));

			if (commonAncestorType != null && commonAncestorType.HasPropertyAtPath(sourceField))
			{
				mappedField = sourceField;
				return true;
			}

			mappedField = null;

			return mappingsDictionary.TryGetValue(typeof(TSource), out TypesPage<FieldsPage> typesPage) &&
				typesPage.TryGetValue(typeof(TTarget), out FieldsPage fieldsPage) &&
				((FieldsPage<TSource, TTarget>)fieldsPage).TryGetValue(sourceField, out mappedField);
		}

		/// <summary>
		/// adds a field mapping between source model and target model
		/// </summary>
		/// <typeparam name="TSource"></typeparam>
		/// <typeparam name="TTarget"></typeparam>
		/// <param name="sourceField"></param>
		/// <param name="targetField"></param>
		/// <param name="bothWays"></param>
		public void Add<TSource, TTarget>(string sourceField, string targetField, bool bothWays = false)
		{
			var fieldsPage = GetFieldsPage<TSource, TTarget>();

			fieldsPage.AddMappedField(sourceField, targetField);

			if (bothWays)
			{
				var oppositeFieldsPage = GetFieldsPage<TTarget, TSource>();
				oppositeFieldsPage.AddMappedField(targetField, sourceField);
			}
		}

		/// <summary>
		/// adds the complex mapping action between source and target model
		/// </summary>
		/// <typeparam name="TSource"></typeparam>
		/// <typeparam name="TTarget"></typeparam>
		/// <param name="functionMapping"></param>
		public void AddFunctionMapping<TSource, TTarget>(FunctionMapping<TSource, TTarget> functionMapping)
		{
			var fieldsPage = GetFieldsPage<TSource, TTarget>();

			fieldsPage.FunctionMappings.Add(functionMapping);
		}

		/// <summary>
		/// gets the fields page for the given source and target type
		/// </summary>
		/// <typeparam name="TSource"></typeparam>
		/// <typeparam name="TTarget"></typeparam>
		/// <returns></returns>
		public FieldsPage GetFieldsPage(Type sourceType, Type targetType)
		{
			var typesPage = mappingsDictionary.GetValueOrAddedNew(sourceType);

			if (!typesPage.TryGetValue(targetType, out FieldsPage fieldsPage))
			{
				fieldsPage = new FieldsPage(sourceType, targetType, DefaultMappingOptions);
				typesPage.Add(targetType, fieldsPage);

			}

			return fieldsPage;
		}

		/// <summary>
		/// gets the fields page for the given source and target type
		/// </summary>
		/// <typeparam name="TSource"></typeparam>
		/// <typeparam name="TTarget"></typeparam>
		/// <returns></returns>
		public FieldsPage GetFieldsPage<TSource, TTarget>()
		{
			return GetFieldsPage(typeof(TSource), typeof(TTarget));
		}

		/// <summary>
		/// enable mapping of properties by the same name between the given two types
		/// </summary>
		/// <typeparam name="TSource"></typeparam>
		/// <typeparam name="TTarget"></typeparam>
		public void SetOptions<TSource, TTarget>(MappingOptions mappingOptions, bool bothWays = false)
		{
			var fieldsPage = GetFieldsPage<TSource, TTarget>();
			fieldsPage.Options.Merge(mappingOptions);

			if (bothWays)
			{
				var reverseFieldsPage = GetFieldsPage<TTarget, TSource>();
				reverseFieldsPage.Options.Merge(mappingOptions);
			}
		}

		public MappingOptions GetOptions<TSource, TTarget>()
		{
			return GetFieldsPage<TSource, TTarget>().Options;
		}

		public void SetMappingDepth<TSource, TTarget>(int depth)
		{
			var fieldsPage = GetFieldsPage<TSource, TTarget>();
			fieldsPage.Depth = depth;
		}
	}
}
