﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace PolyMapper
{
	/// <summary>
	/// fields page for model mapping
	/// </summary>
	public class FieldsPage
	{
		public int? Depth = null;

        public MappingOptions Options { get; set; }

		/// <summary>
		/// field mappings
		/// </summary>
		public Dictionary<string, string> PropertyMappings = new Dictionary<string, string>();

		/// <summary>
		/// list of mapping actions based on logic more complex than simply reading and writing properties at specified paths
		/// </summary>
		public List<FunctionMapping> FunctionMappings = new List<FunctionMapping>();

		public Type SourceType;
		public Type TargetType;

		public FieldsPage(Type sourceType, Type targetType, MappingOptions defaultMappingOptions)
		{
            Options = defaultMappingOptions;
			SourceType = sourceType;
			TargetType = targetType;
		}

		/// <summary>
		/// try to get the mapped value of a given property
		/// </summary>
		/// <param name="sourceProperty"></param>
		/// <param name="targetProperty"></param>
		/// <returns></returns>
		public bool TryGetValue(string sourceProperty, out string targetProperty, MappingOptions mappingOptions = null)
		{
			mappingOptions = mappingOptions ?? Options;

			if (PropertyMappings.TryGetValue(sourceProperty, out targetProperty))
			{
				return true;
			}
			else
			{
				if (mappingOptions.SameNameMapping != SameNameMappingMode.Disabled)
				{
					var targetProperties = TargetType.GetProperties(BindingFlags.Public | BindingFlags.Instance);

					var caseSensitiveNameMatch = targetProperties.Select(p => p.Name).FirstOrDefault(n => String.Equals(sourceProperty, n, StringComparison.Ordinal));

					if (caseSensitiveNameMatch != null)
					{
						targetProperty = caseSensitiveNameMatch;
						return true;
					}
					else if (mappingOptions.SameNameMapping == SameNameMappingMode.IgnoreCase)
					{
						var ignoreCaseNameMatch = targetProperties.Select(p => p.Name).FirstOrDefault(n => String.Equals(sourceProperty, n, StringComparison.OrdinalIgnoreCase));

						if (ignoreCaseNameMatch != null)
						{
							targetProperty = ignoreCaseNameMatch;
							return true;
						}
					}
				}
				return false;
			}
		}

		/// <summary>
		/// add a mapped field to the fields page
		/// </summary>
		/// <param name="sourceProperty"></param>
		/// <param name="targetProperty"></param>
		public void AddMappedField(string sourceProperty, string targetProperty)
		{
			if (PropertyMappings.TryGetValue(sourceProperty, out string existingTargetProperty))
			{
				PropertyMappings[sourceProperty] = targetProperty;
			}
			else
			{
				PropertyMappings.Add(sourceProperty, targetProperty);
			}
		}

		/// <summary>
		/// get all mappings based on name equivalence
		/// </summary>
		/// <returns></returns>
		public Dictionary<string, string> GetSameNameMappings(MappingOptions mappingOptions = null)
		{
			mappingOptions = mappingOptions ?? Options;

			var commonPropertiesToBeMapped = new Dictionary<string, string>();

			if (mappingOptions.SameNameMapping != SameNameMappingMode.Disabled)
			{
				foreach (var sourceName in SourceType.GetProperties(BindingFlags.Public | BindingFlags.Instance).Select(p => p.Name))
				{
					if (TryGetValue(sourceName, out string targetName))
					{
						commonPropertiesToBeMapped.Add(sourceName, targetName);
					}
				}
			}

			return commonPropertiesToBeMapped;
		}
	}

	/// <summary>
	/// fields page for model mapping
	/// </summary>
	public class FieldsPage<TSource, TTarget> : FieldsPage
	{
		public FieldsPage(MappingOptions defaultMappingOptions) : base(typeof(TSource), typeof(TTarget), defaultMappingOptions)
		{
		}
	}


}
