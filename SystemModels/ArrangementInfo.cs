﻿using System.Collections.Generic;

namespace PolyMapper.SystemModels
{
	public class ArrangementInfo
	{
		public PagingInfo pagingInfo { get; set; }
		public List<Filter> filters { get; set; }

		public ArrangementInfo()
		{
			filters = new List<Filter>();
		}

		public void reset()
		{
			this.pagingInfo.pageIndex = 0;
			this.filters = new List<Filter>();
		}
	}
}
