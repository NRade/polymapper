﻿namespace PolyMapper.SystemModels
{
	public class Filter
	{
		public string field { get; set; }
		public object value { get; set; }
		public FilterMode mode { get; set; }
	}

	public enum FilterMode
	{
		Contains,
		IsOneOf
	}
}
