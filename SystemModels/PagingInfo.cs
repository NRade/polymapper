﻿using System.Collections.Generic;
using System.Linq;

namespace PolyMapper.SystemModels
{
	public class PagingInfo
	{
		public int lastPage { get; set; }

		public int pageSize { get; set; }

		public int pageIndex { get; set; }

		public string pageStyle { get; }

		public PagingInfo()
		{

		}

		public PagingInfo(int pageSize, int pageIndex, string pageStyle = "Isolated")
		{
			this.pageSize = pageSize;
			this.pageIndex = pageIndex;
			this.pageStyle = pageStyle;
		}

		public int startIndex
		{
			get
			{
				return pageSize * pageIndex;
			}
		}

		public int endIndex
		{
			get
			{
				return startIndex + pageSize;
			}
		}
	}

	public static class PagingInfoExtensions
	{
		public static IEnumerable<T> TakePage<T>(this IEnumerable<T> unfilteredEnumerable, PagingInfo pagingInfo)
		{
			if (pagingInfo == null)
			{
				return unfilteredEnumerable;
			}

			return unfilteredEnumerable.Skip(pagingInfo.startIndex).Take(pagingInfo.pageSize);
		}
	}
}
