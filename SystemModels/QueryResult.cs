﻿using System.Collections.Generic;

namespace PolyMapper.SystemModels
{
	public class QueryResult<TData>
	{
		public List<TData> data { get; set; }

		public ArrangementInfo arrangementInfo { get; set; }
	}
}
