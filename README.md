# PolyMapper

PolyMapper is a reflection based mapper capable of transferring property values between classes of different types.

A PolyMapper instance is configured upon initialization and then used to map values between object instances.

# Type Mapping Initialization

Configuring ModelMapper works with two types of mappings: fieldmappings (simple mappings) and functionmappings (complex mappings).

FieldMappings are mappings where the values of properties between two classes can be transferred simply by reading the value from the property of one model and depositing it in the corresponding property of another. These mappings always work in both directions.

FieldMappings can be specified between multiple classes at once. In the following example, mappings are defined between three classes:

![alt text](https://i.imgur.com/jnnxSOn.png)

The names of properties are provided in the form of Expressions so that intellisense validates the input. Properties can be more than one level deep in the object, such as in the case of “_po.requisitionId_PORequisitions.subjectId_POBannerOrders.eurofinsOrderKey” as seen above.

FunctionMappings are specified between only two classes at a time and work only in one direction. Instead of simply identifying properties that are mappable between classes, these specify how the transfer of values is made using a function delegate.

![alt text](https://i.imgur.com/eSXYEPt.png)

FunctionMappings can be specified between lists of objects, which may have performance advantages. This defined a mapping between multiple instances, but still between only two class types in one direction. In the following example, lists of objects to be looked up are retrieved from the database once in the act of mapping two lists of objects. This prevents the need to do the performance-intensive database queries every time a pair of objects is mapped.

![alt text](https://i.imgur.com/mcoSDTa.png)

# Mapping Usage

Once a PolyMapper has been configured, it can be used to map object instances in several ways.

The public MapModels method takes a list of source objects and an existing list of target objects  and executes all configured mappings between the objects in the two lists.
The MapModel method does the same between a source object and an existing target object.

The CreateModels method takes a list of source objects, creates a list of target objects and then maps property values from the source list to the target list the way MapModels does.
CreateModel method does the same for a single source object, creating a single target object.

# Mapping Options

ModelMapper’s behavior can be finetuned by specifying MappingOptions. These can be defined on three levels:

* Default MappingOptions for the entire mapper instance, between all types it is configured for
* Default MappingOptions for the mapping between two specific types
* Specific MappingOptions for a single mapping action

MappingOptions consist of the following defineable properties:

SameNameMapping: defines whether properties with the same names should be mapped by default between two objects and whether this should be done on a case-sensitive basis or not. Possible values: caseSensitive, IgnoreCase, Disabled.

ListMapping: defines how the mapper handles lists of objects when these should be mapped between two objects. Possible values:

* MapByValues: each object in the list is re-created and given an equivalent value, possibly mapped using mappings configured in the mapper. The list in the source object and the list in the target object do not contain references to the same objects.
* MapByReference: references to objects in the list are copied and only one instance of each object in the list remains in existence.
* Ignore: refrain from mapping lists objects entirely (possibly for performance reasons or to prevent infinite regressions)

ReferenceTypeMapping: defines how the mapper handles properties with reference types when these should be mapped between two objects. Possible values:

* Clone: the object is re-created and given an equivalent value, possibly mapped using mappings configured in the mapper.
* MapByReference: a new reference to the same object is created in the target object and the object itself is not recreated.
* Ignore: refrain from mapping reference types entirely

MappingDepth: integer that defines how many levels down lists of mappable objects should be mapped. By default this depth is infinite (if ListMapping is not set to Ignore), but it can be lowered to improve performance or prevent infinite regressions.

Default values of the settings are as follows:

![alt text](https://i.imgur.com/Ze155JJ.png)

